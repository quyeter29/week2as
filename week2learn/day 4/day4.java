import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Function;

/**
 * day1
 */
public class day4 {

    public static boolean ArrayIndexOutOfBoundsException (int i,int[] ae)
    {
        if (i < 0 || i > ae.length)
        {
           System.out.println("Your index not available !");
           return false;
        }
        return true;
     
    }
    public static void main(String[] args) {
      
    }
    public static void Q1(){
        Scanner sc=new Scanner(System.in);
        System.out.println("how many element you want to add ?");
        int n =sc.nextInt();
        int[] intArray = new int[n];
        for (int i = 0; i < intArray.length; i++) {
            int check=0;
                while (check==0) {
                    try {
                        System.out.println("Enter element num :"+(i+1));
                        int num=sc.nextInt();
                        intArray[i]=num;
                        check=1;
                    } catch (Exception e) {
                        System.out.println("your input not an int");
                    }
                   
                }
        }
        int ind=-1;
        do {
            System.out.println("Enter index of number you wanna print out");
            ind =sc.nextInt();
        } while (!ArrayIndexOutOfBoundsException(ind,intArray));
        System.out.print("Value of index num "+ind +" : ");
        System.out.println(intArray[ind]);
       
        
        sc.close();
    }
    public static void q2()
    {
        
    }
}