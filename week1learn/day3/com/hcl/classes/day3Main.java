package com.hcl.classes;

import java.util.ArrayList;
import java.util.Scanner;

public class day3Main {
    public static ArrayList<task> arrTasks = new ArrayList<>();

    static void menu()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose option :");
        System.out.println("1.Add Task:");
        System.out.println("2.Update Task :");
        System.out.println("3.Search Task :");
        System.out.println("4.Assignee Task :");
        System.out.println("0.Exit app :");
        String key = sc.next();
        task t = new task();
        switch (key) {
            case "1":
                t.addTask(arrTasks);
                menu();
                break;
            case "2":
                t.updateTask(arrTasks);
                menu();
                break;
            case "3":
                t.searchTask(arrTasks);
                menu();
                break;
            case "4":
                t.assigneeTask(arrTasks);
                menu();
                break;
            case "0":
                System.exit(0);
                menu();
                break;
            default:
                break;
        }
        sc.close();

    }
    public static void main(String[] args) {
     menu(); 
    }
}
