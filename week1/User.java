

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


public class User {

	Store magic = new Store();
	Scanner sc = new Scanner(System.in);

	week1 week1 = new week1();
	public void Menu(ArrayList<BookClass> arrBook, ArrayList<UserClass> arrUser, String Username ) {
		System.out.println("1. Display list book");
		System.out.println("2. Display my favorite book ");
		System.out.println("3. Search book by id ");
		System.out.println("4. Sign out ");
		System.out.println("0. Exit ");
		System.out.print("Enter your choice: ");
		int ch = Integer.parseInt(sc.nextLine());
		System.out.println();
		switch (ch) {
		case 0:

			System.exit(0);
			break;
		case 1:
			DisplayListBook(arrBook);
			Menu(arrBook, arrUser, Username);
			break;
		case 2:
			DisplayFavoriteBook(arrBook, arrUser, Username);
			Menu(arrBook, arrUser, Username);
			break;
		case 3:
			SearchBookbyID(arrBook, Username, arrUser);
			Menu(arrBook, arrUser, Username);
			break;
		case 4:
			week1.Login(arrUser, arrBook);
			break;
		default:
			System.out.println("Enter int format");
			week1.Login(arrUser, arrBook);
			break;
		}
	}

	public void DisplayListBook(ArrayList<BookClass> arrBook) {

		for(BookClass book : arrBook) {
			System.out.println(book.toString());
		}
	}

	public void DisplayFavoriteBook(ArrayList<BookClass> arrBook, ArrayList<UserClass> arrUser, String username) {
		for (UserClass user : arrUser) {
			if (user.getUserName().trim().equals(username)) {
				for(int i : user.getFavourite()) {
					for(int j = 0; j< arrBook.size(); j++) {
						if (arrBook.get(j).getBookId() == i) {
							System.out.println(arrBook.get(j).toString());
						}
					}
				}
			}
		}
	}

	public void SearchBookbyID(ArrayList<BookClass> arrBook, String username, ArrayList<UserClass> arrUser) {
		System.out.print("Enter id you want to search: ");
		int id = sc.nextInt();
		System.out.println();
		magic.SearchbyID(arrBook, id);
		System.out.print("Enter 'Y' to see detail");
		String tmp = sc.nextLine();
		if (tmp.trim().toUpperCase().equals("Y")) {
			bookID(arrBook, id);
		}
		System.out.print("Enter 'Y' to add book in list favorite book ");
		String tmpString = sc.nextLine();
		if (tmpString.trim().toUpperCase().equals("Y")) {
			for (UserClass user : arrUser) {
				if (user.getUserName().trim().equals(username)) {
					user.getFavourite().add(id);
				}
			}
		}
	}
	
	public void bookID(ArrayList<BookClass> arrBook, int id) {
		bookID(arrBook, id);
	}
}
